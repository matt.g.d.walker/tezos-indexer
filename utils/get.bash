#!/bin/bash
# Open Source License
# Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
# Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

set -x
NODE_URL=${NODE_URL:-"http://localhost:8732/"}
top=$(curl -s $NODE_URL/chains/main/blocks/head|jq .header.level)
(( top = top / 1000 ))
for ((i=0; i < top ; i++))
do
  date
  ((start=1000*i+1)) ;
  # /!\ existing files are NOT overwritten /!\
  [[ -f $(printf "%07d" $start).tzblocks ]] || \
  curl -s $NODE_URL/chains/main/blocks/'['$start-$((start+999))']' > $(printf "%07d" $start).tzblocks ;
done
set +x
