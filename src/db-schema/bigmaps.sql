-- Open Source License
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


SELECT 'bigmaps.sql' as file;

DO $$ --SEQONLY
BEGIN --SEQONLY
  IF EXISTS (SELECT * FROM pg_tables WHERE tablename = 'indexer_version') --SEQONLY
     AND EXISTS (SELECT * FROM indexer_version WHERE not multicore) --SEQONLY
     AND EXISTS (SELECT * FROM pg_tables WHERE tablename = 'bigmap' AND schemaname = 'c') --SEQONLY
  THEN --SEQONLY
    ALTER TABLE c.bigmap ALTER COLUMN i TYPE bigint; --SEQONLY
  END IF; --SEQONLY
END; --SEQONLY
$$; --SEQONLY


-- Table `C.bigmap` uses an storage method similar or equal to "copy-on-write",
-- to allow reorganisations to happen seemlessly while offering fast access to any
-- big map at any block level.
-- Depending on what your queries are, you might want to create additional indexes.
CREATE TABLE IF NOT EXISTS C.bigmap (
     id bigint
   , "key" jsonb -- key can be null because of allocs
   , key_hash char(54) -- key_hash can be null because key can be null
   , "key_type" jsonb
   , "value" jsonb -- if null, then it means it was deleted, or not filled yet
   , "value_type" jsonb
   , block_hash_id int not null
   , block_level int not null
   , operation_id bigint not null
   , sender_id bigint not null
   , receiver_id bigint not null
   , name text -- not used (yet)
   , i bigint not null -- i means the i-th bigmapdiff met in the block, except if it comes from a COPY instruction
   -- if i results of a COPY, then i is negative. Cf. FUNCTION B.copy for more details
   , kind smallint not null -- 0: alloc, 1: update, 2: clear, 3: copy,
   , annots text
   , strings text[]
   , uri int[]
   , contracts bigint[]
);
--PKEY bigmap_pkey; C.bigmap; block_hash_id, i --SEQONLY
--FKEY bigmap_block_hash_block_level_fkey; C.bigmap; block_hash_id, block_level; C.block(hash_id, level); CASCADE --SEQONLY
--FKEY bigmap_operation_id_fkey; C.bigmap; operation_id; C.operation_alpha(autoid); CASCADE --SEQONLY
--FKEY bigmap_sender_fkey; C.bigmap; sender_id; C.addresses(address_id); CASCADE --SEQONLY
--FKEY bigmap_receiver_fkey; C.bigmap; receiver_id; C.addresses(address_id); CASCADE --SEQONLY

CREATE INDEX IF NOT EXISTS bigmap_block_hash on C.bigmap using btree (block_hash_id); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_block_level on C.bigmap using btree (block_level); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_operation_id on C.bigmap using btree (operation_id); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_key_hash on C.bigmap using btree (key_hash); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_key on C.bigmap using btree ("key"); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_name on C.bigmap using btree (name); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_id on C.bigmap using btree (id); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_sender on C.bigmap using btree (sender_id); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_receiver on C.bigmap using btree (receiver_id); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_annots on C.bigmap using btree (annots); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS bigmap_key_type on C.bigmap using btree ("key_type"); --SEQONLY
--OPT CREATE INDEX IF NOT EXISTS bigmap_value_type on C.bigmap using btree ("value_type"); --SEQONLY
--CREATE INDEX IF NOT EXISTS bigmap_strings on C.bigmap using GIN (strings); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_uri on C.bigmap using GIN (uri); --SEQONLY
CREATE INDEX IF NOT EXISTS bigmap_contracts on C.bigmap using GIN (contracts); --SEQONLY



CREATE OR REPLACE FUNCTION B.get_by_id (xid bigint)
returns table (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_hash char, block_level int, i bigint)
as $$
with r as
(select id, "key", key_hash, key_type, "value", value_type, block_hash(block_hash_id), block_level, i
 from C.bigmap b where b.id = xid
 and block_level = (select block_level from C.bigmap where id = xid order by block_level desc limit 1) order by i desc)
select * from r where "value" is not null;
$$ language SQL stable;


CREATE OR REPLACE FUNCTION B.get_by_key_hash (xkey_hash char) -- results are undefined if xkey_hash is null
returns table (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_hash char, block_level int, i bigint)
as $$
DECLARE id bigint := (select id from C.bigmap where key_hash = xkey_hash order by block_level desc, i desc limit 1);
BEGIN
with r as
(select * from B.get_by_id(id))
select * from r where key_hash = xkey_hash;
END
$$ language PLPGSQL;


CREATE OR REPLACE FUNCTION B.assoc (xid bigint, xkey jsonb)
returns table ("key" jsonb, "value" jsonb, block_hash char)
as $$
select "key", "value", block_hash(block_hash_id)
from C.bigmap
where id = xid and "key" = xkey
and block_hash_id = (select b.block_hash_id from C.bigmap b where xkey = b.key order by b.block_level desc limit 1);
$$ language SQL stable;


DROP FUNCTION IF EXISTS B.update (xid bigint, xkey jsonb, xkey_hash char, xvalue jsonb, xblock_hash int, xblock_level int, xsender bigint, xreceiver bigint, xi smallint, opaid bigint, xstrings text[]);
CREATE OR REPLACE FUNCTION B.update (xid bigint, xkey jsonb, xkey_hash char, xvalue jsonb, xblock_hash int, xblock_level int, xsender bigint, xreceiver bigint, xi smallint, opaid bigint, xstrings text[], max_length smallint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_hash_id, block_level, sender_id, receiver_id, i, operation_id, kind, strings
  , uri, contracts --SEQONLY
)
values (xid, xkey, xkey_hash, xvalue, xblock_hash, xblock_level, xsender, xreceiver, xi, opaid, 1, xstrings
  , (select extract_uris(xstrings, max_length)) --SEQONLY
  , (select extract_contracts(xstrings, opaid)) --SEQONLY
)
on conflict do nothing; --CONFLICT
$$ language SQL;


CREATE OR REPLACE FUNCTION B.clear (xid bigint, xblock_hash int, xblock_level int, xsender bigint, xreceiver bigint, xi smallint, opaid bigint)
returns void
as $$
insert into C.bigmap (id, "key", key_hash, "value", block_hash_id, block_level, sender_id, receiver_id, i, operation_id, kind)
values (xid, null, null, null, xblock_hash, xblock_level, xsender, xreceiver, xi, opaid, 2)
on conflict do nothing; --CONFLICT
$$ language SQL;


CREATE OR REPLACE FUNCTION B.alloc (xid bigint, xkey_type jsonb, xvalue_type jsonb, xblock_hash int, xblock_level int, xsender bigint, xreceiver bigint, xi smallint, opaid bigint, xannots text)
returns void
as $$
insert into C.bigmap (id, "key_type", value_type, block_hash_id, block_level, sender_id, receiver_id, i, operation_id, kind, annots)
values (xid, xkey_type, xvalue_type, xblock_hash, xblock_level, xsender, xreceiver, xi, opaid, 0, xannots)
on conflict do nothing; --CONFLICT
$$ language SQL;


CREATE OR REPLACE FUNCTION B.get_by_id_for_copy (xid bigint, xblock_level int)
returns table (id bigint, "key" jsonb, key_hash char, key_type jsonb, "value" jsonb, value_type jsonb, block_hash char, block_level int, i bigint, annots text, strings text[], uri int[], contracts bigint[])
as $$
with r as
(select id, "key", key_hash, key_type, "value", value_type, block_hash(block_hash_id), block_level, i, annots, strings, uri, contracts
 from C.bigmap b where b.id = xid
 and block_level = (select block_level from C.bigmap where id = xid and block_level <= xblock_level order by block_level desc limit 1) order by i desc)
select * from r where "value" is not null;
$$ language SQL stable;

CREATE SEQUENCE IF NOT EXISTS C.bigmap_serial START 1;

CREATE OR REPLACE FUNCTION B.copy (xid bigint, yid bigint, bhid int, xblock_level int, xsender bigint, xreceiver bigint, i bigint, opaid bigint)
returns void
as $$
--     9 007 199 254 740 991 is max int53
-- 9 223 372 036 854 775 807 is max int64
BEGIN
IF bhid > 0 THEN
  PERFORM setval('c.bigmap_serial', bhid::bigint * 1000000000::bigint + i::bigint * 100000::bigint);
ELSE
  -- This is a workaround: all negative bhid are deleted from the DB eventually.
  PERFORM setval('c.bigmap_serial', -bhid::bigint * 1000000000::bigint + i::bigint * 100000::bigint);
END IF;
INSERT INTO C.bigmap (id, "key", key_hash, "key_type", "value", value_type, block_hash_id, block_level, sender_id, receiver_id, i, operation_id, kind, annots, strings, uri, contracts)
SELECT yid, "key", key_hash, "key_type", "value", value_type, bhid, xblock_level, xsender, xreceiver, -nextval('c.bigmap_serial'), opaid, 3, annots, strings, uri, contracts
FROM B.get_by_id_for_copy (xid, xblock_level)
ON CONFLICT DO NOTHING;
END;
$$ LANGUAGE PLPGSQL;
-- bhid * 1000000000 --> ([1-9] * 10^6) * 10^9 -> bhid can go up to 9*10^6 with int53, or 9*10^9 with int64 -- both are fine for now
-- Going at 10 blocks per minute (instead of 1), we'd be good with int53 up to about end of September 2022.
-- At the rythm of 1 block / min, this holds up to after year 2030.
-- i * 100000 --> i*10^5 --> the size of one bigmap is limited to 100K (0-99,999),
-- the number of bigmap diffs per block is limited to 99,999
-- If at some point it no longer fits, we'll change the numbers to fully use 64-bit integers.



-- There is no way for bigmaps to be fully "indexed by segments" because
-- we may record a bigmap copy without having access to the original bigmap.
-- Therefore we create a table that lists blocks having such diffs, so that we may
-- re-run the indexer only on those blocks to properly record copies.
CREATE TABLE IF NOT EXISTS C.bigmap_copies ( --MULTICORE
  block_level int primary key --MULTICORE
); --MULTICORE
CREATE TABLE IF NOT EXISTS C.bigmap_delayed_copies ( --MULTICORE
  xid bigint not null, yid bigint not null, bhid int not null, xblock_level int not null, xsender bigint not null, xreceiver bigint not null, i smallint not null, opaid bigint not null --MULTICORE
); --MULTICORE

CREATE OR REPLACE FUNCTION B.copy (xid bigint, yid bigint, bhid int, xblock_level int, xsender bigint, xreceiver bigint, i bigint, opaid bigint) --MULTICORE
returns void --MULTICORE
as $$ --MULTICORE
insert into C.bigmap_delayed_copies values (xid, yid, bhid, xblock_level, xsender, xreceiver, i, opaid); --MULTICORE
$$ language SQL; --MULTICORE

DO $$ --SEQONLY
BEGIN --SEQONLY
  IF EXISTS (SELECT true FROM pg_tables where tablename = 'indexer_version') --SEQONLY
     and exists (select * from indexer_version where not multicore) --SEQONLY
     AND EXISTS (SELECT * FROM pg_tables WHERE tablename = 'bigmap_delayed_copies' AND schemaname = 'c') --SEQONLY
  THEN --SEQONLY
    PERFORM B.copy (xid, yid, bhid, xblock_level, xsender, xreceiver, i, opaid) from C.bigmap_delayed_copies order by xblock_level asc; --SEQONLY
    DELETE FROM C.bigmap_delayed_copies; --SEQONLY
  END IF; --SEQONLY
END; --SEQONLY
$$; --SEQONLY
