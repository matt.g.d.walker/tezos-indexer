-- Open Source License
-- Copyright (c) 2019-2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

\set ON_ERROR_STOP on

SELECT 'versions.sql' as file;


DO $$
BEGIN
  IF EXISTS (SELECT 1 FROM pg_tables where tablename = 'block')
  AND NOT EXISTS (SELECT 1 FROM pg_tables where tablename = 'indexer_version') THEN
    raise 'You seem to be running a non compatible version of the indexer';
  END IF;
END
$$;

----------------------------------------------------------------------
-- VERSION HISTORY
----------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS indexer_version (
     version text primary key -- the current version
   , new_tables text not null -- the most recent version where new tables were introduced
   , new_columns text not null -- the most recent version where new columns were introduced
   , alter_types text not null -- the most recent version where some types were altered
   , build text -- placeholder for now
   , dev bool not null -- should be set to true, except for released versions
   , multicore bool not null
   , conversion_in_progress bool not null default false
   , autoid SERIAL UNIQUE
);

DO $$
BEGIN
ALTER TABLE indexer_version ADD COLUMN conversion_in_progress bool not null default false;
EXCEPTION WHEN OTHERS THEN return;
END $$;

-- N.B. 9.3.d and 9.3.e are the same
CREATE OR REPLACE FUNCTION update_indexer_version ()
RETURNS VOID
AS $$
BEGIN
  INSERT INTO indexer_version (version,new_tables,new_columns,alter_types,build,dev,multicore) VALUES (
     '9.3.f' -- version
   , '9.3.0' -- new_tables
   , '9.3.a' -- new_columns
   , '9.3.b' -- alter_types
   , '' -- build
   , false -- dev
   , true -- multicore
   ) ON CONFLICT (version) DO NOTHING;
END
$$ LANGUAGE PLPGSQL;

DO $$
BEGIN
  IF (SELECT count(*) FROM indexer_version WHERE version < '9.3.0') > 0
  THEN
    RAISE 'You already have a non-compatible schema.';
  END IF;
  IF (SELECT count(*) FROM indexer_version WHERE NOT multicore) > 0         --MULTICORE
  THEN                                                                      --MULTICORE
    RAISE 'You cannot convert a non-multicore schema to a multicore one.';  --MULTICORE
  END IF;                                                                   --MULTICORE
  IF (SELECT count(*) FROM indexer_version WHERE conversion_in_progress) > 0
  THEN
    RAISE 'You have a corrupted DB: it was corrupted at creation time or it was broken during a conversion from multicore mode to default mode';
  END IF;
  PERFORM update_indexer_version (); --MULTICORE
  UPDATE indexer_version SET conversion_in_progress = true; --SEQONLY
END $$;
