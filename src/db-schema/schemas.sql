-- Open Source License
-- Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

SELECT 'schemas.sql' as file;

-- Note that although we use upper case for schema names in our code,
-- Postgres doesn't see them any differently than if they were in lower case.
-- Moreover, if you are using psql and want autocompletion, upper case may not work
-- and you may have to use lower case notation.
-- The upper case is used to facilitate refactoring when need be and reading.


-- Blockchain's core data
create schema if not exists C;

-- Insertion functions
create schema if not exists I;

-- Pre-Insertion functions (insertion of incomplete rows)
create schema if not exists H;

-- Update functions
create schema if not exists U;

-- Get functions
create schema if not exists G;

-- Mempool
create schema if not exists M;

-- Tokens
create schema if not exists T;

-- Bigmaps
create schema if not exists B;
