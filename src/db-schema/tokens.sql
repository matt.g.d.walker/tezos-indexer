-- Open Source License
-- Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- Lines starting with --OPT may be automatically activated
-- Lines ending with --OPT may be automatically deactivated

-- DB schema for tokens operations

SELECT 'tokens.sql' as file;



CREATE TABLE IF NOT EXISTS T.contract (
  address_id bigint not null,
  block_hash_id int not null,
  autoid SERIAL UNIQUE
  , primary key(address_id)
--  , foreign key(address_id, block_hash_id) references C.contract(address_id, block_hash_id) on delete cascade
  , foreign key(block_hash_id) references C.block(hash_id) on delete cascade
);
--pkey contract_pkey; T.contract; address_id


CREATE OR REPLACE FUNCTION T.I_contract (a bigint, b int)
returns void
as $$
insert into T.contract (address_id, block_hash_id)
values (a, b)
on conflict do nothing; --CONFLICT
$$ language sql;

CREATE INDEX IF NOT EXISTS token_contract_address on T.contract using btree(address_id);
CREATE INDEX IF NOT EXISTS token_contract_block_hash on T.contract using btree(block_hash_id);
CREATE INDEX IF NOT EXISTS token_contract_autoid on T.contract using btree(autoid);



CREATE TABLE IF NOT EXISTS T.balance (
  token_address_id bigint not null,
  address_id bigint not null,
  amount smallint,
  autoid SERIAL UNIQUE
  , primary key (token_address_id, address_id)
  , foreign key (address_id) references C.addresses(address_id) on delete cascade
);
--FKEY t_balance_token_address_id_fkey; T.balance; token_address_id; T.contract(address_id); CASCADE --SEQONLY


CREATE INDEX IF NOT EXISTS token_balance_token on T.balance using btree(token_address_id);
CREATE INDEX IF NOT EXISTS token_balance_address on T.balance using btree(address_id);
CREATE INDEX IF NOT EXISTS token_balance_autoid on T.balance using btree(autoid);



CREATE OR REPLACE FUNCTION T.I_balance (ta bigint, a bigint, am smallint)
returns void
as $$
-- insert into addresses values (a) on conflict do nothing;
insert into T.balance (token_address_id, address_id, amount)
values (ta, a, am)
on conflict (token_address_id, address_id) do  --CONFLICT
update set amount = am where T.balance.token_address_id = ta and T.balance.address_id = a;  --CONFLICT2
$$ language sql;

DO $$
BEGIN
  IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'token_operation_kind') THEN
    CREATE TYPE token_operation_kind AS ENUM ('transfer', 'approve', 'getBalance', 'getAllowance', 'getTotalSupply');
  END IF;
END
$$;



CREATE TABLE IF NOT EXISTS T.operation (
  operation_id bigint not null primary key,
  token_address_id bigint not null,
  caller_id bigint not null,
  kind token_operation_kind not null,
  autoid SERIAL UNIQUE, -- this field should always be last
  foreign key (operation_id) references C.operation_alpha(autoid) on delete cascade,
  foreign key (caller_id) references C.addresses(address_id) on delete cascade
);
CREATE INDEX IF NOT EXISTS token_operation_kind    on T.operation using btree (kind); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_operation_autoid  on T.operation using btree (autoid); --OPT --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_operation_caller  on T.operation using btree (caller_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_operation_address on T.operation using btree (token_address_id); --BOOTSTRAPPED
--FKEY t_operation_token_address_id_fkey; T.operation; token_address_id; T.contract(address_id); CASCADE --SEQONLY


CREATE OR REPLACE FUNCTION T.I_operation (opaid bigint, ta bigint, c bigint, k token_operation_kind)
returns void
as $$
insert into T.operation
(operation_id, token_address_id, caller_id, kind)
values (opaid, ta, c, k)
on conflict (operation_id) do nothing; --CONFLICT
$$ language sql;



CREATE TABLE IF NOT EXISTS T.transfer (
  operation_id bigint not null primary key,
  source_id bigint not null,
  destination_id bigint not null,
  amount numeric not null,
  autoid SERIAL UNIQUE,
  foreign key (operation_id) references T.operation(operation_id) on delete cascade,
  foreign key (source_id) references C.addresses(address_id) on delete cascade,
  foreign key (destination_id) references C.addresses(address_id) on delete cascade
);

CREATE INDEX IF NOT EXISTS token_transfer_source         on T.transfer using btree (source_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_transfer_destination    on T.transfer using btree (destination_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_transfer_autoid         on T.transfer using btree (autoid); --OPT --BOOTSTRAPPED


CREATE OR REPLACE FUNCTION T.I_transfer (opaid bigint, s bigint, d bigint, a numeric)
returns void
as $$
insert into T.transfer (operation_id, source_id, destination_id, amount)
values (opaid, s, d, a)
on conflict (operation_id) do nothing; --CONFLICT
$$ language sql;



CREATE TABLE IF NOT EXISTS T.approve (
  operation_id bigint not null primary key,
  address_id bigint not null,
  amount numeric not null,
  autoid SERIAL UNIQUE,
  foreign key (operation_id) references T.operation(operation_id) on delete cascade,
  foreign key (address_id) references C.addresses(address_id) on delete cascade
);

CREATE INDEX IF NOT EXISTS token_approve_address           on T.approve using btree (address_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_approve_autoid            on T.approve using btree (autoid); --OPT --BOOTSTRAPPED



CREATE OR REPLACE FUNCTION T.I_approve(opaid bigint, a bigint, am numeric)
returns void
as $$
insert into T.approve (operation_id, address_id, amount)
values (opaid, a, am)
on conflict (operation_id) do nothing --CONFLICT
$$ language sql;


CREATE TABLE IF NOT EXISTS T.get_balance (
  operation_id bigint not null primary key,
  address_id bigint not null,
  callback_id bigint not null,
  autoid SERIAL UNIQUE,
  foreign key (operation_id) references T.operation(operation_id) on delete cascade,
  foreign key (address_id) references C.addresses(address_id) on delete cascade,
  foreign key (callback_id) references C.addresses(address_id) on delete cascade
);


CREATE INDEX IF NOT EXISTS token_get_balance_address           on T.get_balance using btree (address_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_balance_callback          on T.get_balance using btree (callback_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_balance_autoid            on T.get_balance using btree (autoid); --OPT --BOOTSTRAPPED


CREATE OR REPLACE FUNCTION T.I_get_balance (opaid bigint, a bigint, c bigint)
returns void
as $$
insert into T.get_balance(operation_id, address_id, callback_id)
values (opaid, a, c)
on conflict (operation_id) do nothing; --CONFLICT
$$ language sql;



CREATE TABLE IF NOT EXISTS T.get_allowance (
  operation_id bigint not null primary key,
  source_id bigint not null,
  destination_id bigint not null,
  callback_id bigint not null,
  autoid SERIAL UNIQUE,
  foreign key (operation_id) references T.operation(operation_id) on delete cascade,
  foreign key (source_id) references C.addresses(address_id) on delete cascade,
  foreign key (destination_id) references C.addresses(address_id) on delete cascade,
  foreign key (callback_id) references C.addresses(address_id) on delete cascade
);


CREATE INDEX IF NOT EXISTS token_get_allowance_source            on T.get_allowance using btree (source_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_allowance_destination       on T.get_allowance using btree (destination_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_allowance_callback          on T.get_allowance using btree (callback_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_allowance_autoid            on T.get_balance using btree (autoid); --OPT --BOOTSTRAPPED


CREATE OR REPLACE FUNCTION T.I_get_allowance (opaid bigint, s bigint, d bigint, c bigint)
returns void
as $$
insert into T.get_allowance(operation_id, source_id, destination_id, callback_id)
values (opaid, s, d, c)
on conflict (operation_id) do nothing; --CONFLICT
$$ language sql;


CREATE TABLE IF NOT EXISTS T.get_total_supply (
  operation_id bigint not null primary key,
  callback_id bigint not null,
  autoid SERIAL UNIQUE
  , foreign key (operation_id) references T.operation(operation_id) on delete cascade
  , foreign key (callback_id) references C.addresses(address_id) on delete cascade
);

CREATE INDEX IF NOT EXISTS token_get_total_supply_callback          on T.get_total_supply using btree (callback_id); --BOOTSTRAPPED
CREATE INDEX IF NOT EXISTS token_get_total_supply_autoid            on T.get_total_supply using btree (autoid); --OPT --BOOTSTRAPPED



CREATE OR REPLACE FUNCTION T.I_get_total_supply(opaid bigint, c bigint)
returns void
as $$
insert into T.get_total_supply(operation_id, callback_id)
values (opaid, c)
on conflict (operation_id) do nothing; --CONFLICT
$$ language sql;



CREATE OR REPLACE FUNCTION T.is_token (t bigint)
returns bool
as $$
select exists (select true from T.contract where address_id = t)
$$ language sql stable;
