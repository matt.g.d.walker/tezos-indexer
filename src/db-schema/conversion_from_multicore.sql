-- Open Source License
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
-- Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

-- This file contains things that must be executed at towards the end of the conversion from multicore mode to sequential mode


CREATE OR REPLACE FUNCTION C.tx_pin_addresses_from_strings (s text[], opaid bigint)
RETURNS VOID
AS $$
 with x as (select unnest(s) as element)
 update c.tx
   set
   uri =
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')),
   contracts =
     (select array_agg(I.address(element::char(36),opaid)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36
     )
   where operation_id = opaid;
$$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION C.origination_pin_addresses_from_strings (s text[], k bigint)
RETURNS VOID
AS $$
 with x as (select unnest(s) as element)
 update c.contract_script
   set
   uri =
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')),
   contracts =
     (select array_agg(I.address(element::char(36),0::bigint)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36
     )
   where address_id = k;
$$ LANGUAGE SQL;


CREATE OR REPLACE FUNCTION b.bigmap_pin_addresses_from_strings (s text[], bhid int, bigmapindex bigint)
RETURNS VOID
AS $$
 with x as (select unnest(s) as element)
 update c.bigmap
   set
   uri =
     (select array_agg(I.uri(element)) from x where char_length(element) < 2048 AND (element ~ '[a-zA-Z][a-zA-Z]*://..*' or element ~ 'tezos-storage:.*')),
   contracts =
     (select array_agg(I.address(element::char(36),0::bigint)) from x where (element like 'tz%' or element like 'KT%') and octet_length(element) >= 36
     )
   where (block_hash_id, i) = (bhid, bigmapindex) ;
$$ LANGUAGE SQL;

-- in multicore mode, there can be race conditions that prevent this treatment, so we run it when we convert the multicore mode to sequential mode
DO $$
BEGIN
IF (select multicore from indexer_version order by autoid desc limit 1)
THEN
PERFORM C.tx_pin_addresses_from_strings(strings, operation_id) FROM c.tx WHERE strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL;
END IF;
END $$ ;
DO $$
BEGIN
IF (select multicore from indexer_version order by autoid desc limit 1)
THEN
PERFORM C.origination_pin_addresses_from_strings(strings, address_id) FROM c.contract_script WHERE strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL;
END IF;
END $$ ;
DO $$
BEGIN
IF (select multicore from indexer_version order by autoid desc limit 1)
THEN
PERFORM b.bigmap_pin_addresses_from_strings(strings, block_hash_id, i) FROM c.bigmap WHERE strings IS NOT NULL AND array_length(strings, 1) > 0 AND uri IS NULL AND contracts IS NULL;
END IF;
END $$ ;
DO $$
BEGIN
IF (select multicore from indexer_version order by autoid desc limit 1)
THEN
INSERT INTO C.contract_balance (address_id, block_hash_id, block_level)
SELECT contract_address_id, block_hash_id, block_hash_id
FROM C.balance_updates_block
ON CONFLICT DO NOTHING;
END IF;
END $$ ;
DO $$
BEGIN
IF (select multicore from indexer_version order by autoid desc limit 1)
THEN
INSERT INTO C.contract_balance (address_id, block_hash_id, block_level)
SELECT contract_address_id, (operation_id/10000000)::integer, (operation_id/10000000)::integer
FROM C.balance_updates_op
ON CONFLICT DO NOTHING;
END IF;
END $$ ;



SELECT update_indexer_version ();
UPDATE indexer_version SET conversion_in_progress = false;
-- the following line should be at the end of this file
UPDATE indexer_version SET MULTICORE = false;
