-- Open Source License
-- Copyright (c) 2019-2021 Nomadic Labs <contact@nomadic-labs.com>
--
-- Permission is hereby granted, free of charge, to any person obtaining a
-- copy of this software and associated documentation files (the "Software"),
-- to deal in the Software without restriction, including without limitation
-- the rights to use, copy, modify, merge, publish, distribute, sublicense,
-- and/or sell copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included
-- in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
-- THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.


-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-- VIEWS
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Views making querying the chain easier when testing/developing, however it costs in performance.
-- These views also make queries over versions prior to v9 easier since they use the names of the tables of v8.
--------------------------------------------------------------------------------

SELECT 'views.sql' as file;


CREATE OR REPLACE VIEW addresses as select * from C.addresses;

CREATE OR REPLACE VIEW block as
select *, block_hash(hash_id) as hash, block_hash(predecessor_id) as predecessor
from C.block b;

CREATE OR REPLACE VIEW "operation" as
select *, block_hash(block_hash_id)
from C.operation;

CREATE OR REPLACE VIEW operation_alpha as
select *
, block_hash(block_hash_id) as block_hash
, operation_hash(hash_id) as hash
from C.operation_alpha;

CREATE OR REPLACE FUNCTION block_hash_alpha(id bigint)
returns char as $$
select block_hash(block_hash_id)
from C.operation
where hash_id = (select hash_id from C.operation_alpha where autoid = id)
$$ language sql stable;

CREATE OR REPLACE VIEW operation_sender_and_receiver as
select *
, block_hash_alpha(operation_id) as block_hash
, operation_hash(operation_hash_id_alpha(operation_id)) as hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(sender_id) as sender
, address(receiver_id) as receiver
from C.operation_sender_and_receiver;

CREATE OR REPLACE VIEW proposal as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(source_id) as source
, proposal(proposal_id) as proposal
from C.proposal;

CREATE OR REPLACE VIEW ballot as
select *
, proposal(proposal_id) as proposal
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(source_id) as source
from C.ballot;

CREATE OR REPLACE VIEW double_endorsement_evidence as
select *
, operation_hash_alpha(operation_id)
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id)
from C.double_endorsement_evidence;

CREATE OR REPLACE VIEW double_baking_evidence as
select *
, operation_hash_alpha(operation_id)
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id)
from C.double_baking_evidence;

CREATE OR REPLACE VIEW manager_numbers as
select *
, operation_hash_alpha(operation_id) as hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
from C.manager_numbers;

CREATE OR REPLACE VIEW endorsement as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
, address(delegate_id) as delegate
from C.endorsement;

CREATE OR REPLACE VIEW seed_nonce_revelation as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, block_hash_alpha(operation_id) as block_hash
from C.seed_nonce_revelation;

CREATE OR REPLACE VIEW block_alpha as
select *
, block_hash(hash_id) as hash
, address(baker_id) as baker
from C.block_alpha;

CREATE OR REPLACE VIEW deactivated as
select *
, block_hash(block_hash_id) as block_hash
, address(pkh_id) as pkh
from C.deactivated;

CREATE OR REPLACE VIEW contract as
select *
, address(address_id) as address
, block_hash(block_hash_id) as block_hash
, address(mgr_id) as mgr
, address(delegate_id) as delegate
, address(preorig_id) as preorig
from C.contract;

CREATE OR REPLACE VIEW contract_balance as
select *
, address(address_id) as address
, block_hash(block_hash_id) as block_hash
from C.contract_balance;


CREATE OR REPLACE FUNCTION operation_id_alpha (opaid bigint)
RETURNS smallint
AS $$
select id from c.operation_alpha where autoid = opaid;
$$ language SQL stable;

CREATE OR REPLACE FUNCTION operation_internal_alpha (opaid bigint)
RETURNS smallint
AS $$
select internal from c.operation_alpha where autoid = opaid;
$$ language SQL stable;


CREATE OR REPLACE VIEW tx as
select *
, block_hash_alpha(operation_id) as block_hash
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, address(source_id) as source
, address(destination_id) as destination
, operation_internal_alpha (operation_id) as internal
from C.tx;

CREATE OR REPLACE VIEW origination as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, address(k_id) as k
, block_hash_alpha(operation_id) as block_hash
from C.origination;

CREATE OR REPLACE VIEW delegation as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, address(pkh_id) as pkh
, block_hash_alpha(operation_id) as block_hash
from C.delegation;

CREATE OR REPLACE VIEW reveal as
select *
, operation_hash_alpha(operation_id) as operation_hash
, operation_id_alpha (operation_id) as op_id
, operation_internal_alpha (operation_id) as internal
, address(source_id) as source
, block_hash_alpha(operation_id) as block_hash
from C.reveal;



CREATE OR REPLACE VIEW balance as
select
block_hash_alpha(b1.operation_id) as block_hash
, operation_hash_alpha(b1.operation_id) as operation_hash
, operation_id_alpha(b1.operation_id) as op_id
, operation_internal_alpha(b1.operation_id) as internal
, b1.balance_kind
, b1.cycle
, b1.diff
, b1.id
, address(b1.contract_address_id) as contract_address
from C.balance_updates_op b1
union
select
block_hash(b2.block_hash_id) as block_hash
, null::char as operation_hash
, null::smallint as op_id
, null::smallint as internal
, b2.balance_kind
, b2.cycle
, b2.diff
, b2.id
, address(b2.contract_address_id) as contract_address
from C.balance_updates_block b2 ;


CREATE OR REPLACE VIEW bigmap as select *, block_hash(block_hash_id) as block_hash, address(sender_id) as sender, address(receiver_id) as receiver from C.bigmap;


CREATE OR REPLACE VIEW light_mempool_operations as
select
  hash,
  first_seen_level,
  last_seen_level,
  (last_seen_timestamp-first_seen_timestamp) as presence,
  status,
  id,
  operation_kind,
  source,
  destination,
  autoid
from M.operation_alpha;

CREATE OR REPLACE VIEW token_contract as select *, address(address_id) as address, block_hash(block_hash_id) as block_hash from T.contract;

CREATE OR REPLACE VIEW token_balance as select *, address(address_id) as address, address(token_address_id) as token_address from T.balance;

CREATE OR REPLACE VIEW token_operation as select *, operation_hash_alpha(operation_id) as operation_hash, address(token_address_id) as token_address, address(caller_id) as caller, block_hash_alpha(operation_id) as block_hash from T.operation;

CREATE OR REPLACE VIEW token_transfer as select *, operation_hash_alpha(operation_id) as operation_hash, address(source_id) as source, address(destination_id) as destination, block_hash_alpha(operation_id) as block_hash from T.transfer;

CREATE OR REPLACE VIEW token_approve as select *, operation_hash_alpha(operation_id) as operation_hash, address(address_id) as address, block_hash_alpha(operation_id) as block_hash from T.approve;

CREATE OR REPLACE VIEW token_get_balance as select *, operation_hash_alpha(operation_id) as operation_hash, address(address_id) as address, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_balance;

CREATE OR REPLACE VIEW token_get_allowance as select *, operation_hash_alpha(operation_id) as operation_hash, address(source_id) as source, address(destination_id) as destination, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_allowance;

CREATE OR REPLACE VIEW token_get_total_supply as select *, operation_hash_alpha(operation_id) as operation_hash, address(callback_id) as callback, block_hash_alpha(operation_id) as block_hash from T.get_total_supply;


-----------------------------------------------------------------------------
-- LEGACY (deprecated, will likely be removed)
-----------------------------------------------------------------------------

CREATE OR REPLACE VIEW balance_full as
  select
   block.level, -- block level
   block_alpha.cycle, -- cycle
   cycle_position, -- position in cycle
   operation_hash, -- operation hash
   op_id, -- index in list of operations
   contract_address, -- address of contract
   balance_kind, -- balance kind
   diff -- balance update
  from block
  natural join block_alpha
  join balance on block.hash = balance.block_hash;


-----------------------------------------------------------------------------

CREATE OR REPLACE VIEW level as
  select
   level,
   hash
  from block
  order by level asc;



-----------------------------------------------------------------------------
-- The following view, contract_legacy, is deactivated by default because it's too slow!
-- This view was to provide compatibility with the old version of the contract table, where addresses where unique

-- CREATE OR REPLACE VIEW contract_legacy as
-- select
-- distinct C.address,
--  C.block_hash,
--  coalesce(C.mgr, (select mgr from contract c2 where c2.mgr is not null and c2.address = C.address order by c2.autoid desc limit 1)) as mgr,
--  coalesce(C.delegate, (select delegate from contract c2 where c2.delegate is not null and c2.address = C.address order by c2.autoid desc limit 1)) as delegate,
--  coalesce(C.spendable, (select spendable from contract c2 where c2.spendable is not null and c2.address = C.address order by c2.autoid desc limit 1)) as spendable,
--  coalesce(C.delegatable, (select delegatable from contract c2 where c2.delegatable is not null and c2.address = C.address order by c2.autoid desc limit 1)) as delegatable,
--  coalesce(C.credit, (select credit from contract c2 where c2.credit is not null and c2.address = C.address order by c2.autoid desc limit 1)) as credit,
--  coalesce(C.preorig, (select preorig from contract c2 where c2.preorig is not null and c2.address = C.address order by c2.autoid desc limit 1)) as preorig,
--  coalesce(C.script, (select script from contract c2 where c2.script is not null and c2.address = C.address order by c2.autoid desc limit 1)) as script,
-- C.autoid from
-- contract c, block b
-- where C.block_hash = b.hash and not b.rejected
-- order by autoid desc;


-----------------------------------------------------------------------------
-- --  tx_full view
-- CREATE OR REPLACE VIEW tx_full as
--   select
--    operation_hash, -- operation hash
--    op_id, -- index in list of operations
--    b.hash as block_hash, -- block hash
--    b.level as level, -- block level
--    b.timestamp as timestamp, -- timestamp
--    source, -- source
--    k1.mgr as source_mgr, -- manager of source
--    destination, -- destination
--    k2.mgr as destination_mgr, -- manager of destination
--    fee, -- fees
--    amount, -- amount transfered
--    parameters, -- parameters to target contract, if any
--    storage, -- storage(cf. table tx)
--    entrypoint -- entrypoint(cf. table tx)
--   from tx
--   join operation on tx.operation_hash = operation.hash
--   join block b on operation.block_hash = b.hash and not b.rejected
--   join contract k1 on tx.source = k1.address
--   join contract k2 on tx.destination = k2.address;
