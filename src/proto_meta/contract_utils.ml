(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let proto = "(**# get PROTO #**)"

(**#X ifdef __META1__ open Tezos_raw_protocol_(**# get PROTO#**)X#**)
(**#X else open Tezos_protocol_(**# get PROTO #**) open Protocol X#**)
open Alpha_context
module Client = Tezos_client_(**# get PROTO #**)

class type rpc_context = (**#X ifdef __META1__ Client.Alpha_client_context.full
     X#**)(**#X else Client.Protocol_client_context.full X#**)

module FA12 = struct

  let check_contract cctxt ~chain ~block ~contract () =
    let cctxt = (cctxt : #rpc_context :> rpc_context) in
    (**#X ifdef __META3__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X elseifdef __PROTO7__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X elseifdef __PROTO6__
       let res =
       Client.Client_proto_fa12.contract_has_fa12_interface
        cctxt ~chain ~block ~contract () in
       X#**)
    (**#X else
       let res = (fun _ _ _ _ -> fail "not implemented")
       cctxt chain block contract in
       X#**)
    res

  type action =
    (**#X ifdef __META_34__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO7__ Client.Client_proto_fa12.action = X#**)
    (**#X elseifdef __PROTO6__ Client.Client_proto_fa12.action = X#**)
    | Transfer of Contract.t * Contract.t * Z.t
    | Approve of Contract.t * Z.t
    | Get_allowance of Contract.t * Contract.t * (Contract.t * string option)
    | Get_balance of Contract.t * (Contract.t * string option)
    | Get_total_supply of (Contract.t * string option)

  let _transfer src dst z = Transfer (src, dst, z)
  let _approve addr z = Approve (addr, z)
  let _get_allowance src dst cb = Get_allowance (src, dst, cb)
  let _get_balance addr cb = Get_balance (addr, cb)
  let _get_total_supply cb = Get_total_supply cb

  let action_of_data entrypoint data : (action, unit) result =
    let action entrypoint =
      (**#X ifdef __META_34__
         let open Tezos_client_(**# get PROTO #**) in
         match Script_repr.force_decode data with
         | exception exn ->
         Printf.eprintf "Exception\n%!"; raise exn
         | Error _ -> Error ()
         | Ok (**# ifdef __PROTO10__ expr #**) (**# else (expr, _) #**) ->
         Format.printf "Action: %s: %a\n%!" entrypoint Michelson_v1_printer.print_expr expr;
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X elseifdef __PROTO7__
         match Script_repr.force_decode data with
         | Error _ -> Error ()
         | Ok (expr, _) ->
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X elseifdef __PROTO6__
         match Script_repr.force_decode data with
         | Error _ -> Error ()
         | Ok (expr, _) ->
          Tezos_micheline.Micheline.root expr
          |> Client.Client_proto_fa12.action_of_expr ~entrypoint
          |> begin function
             | Error _ -> Error ()
             | Ok v -> Ok v
             end
         X#**)
      (**#X else (fun _ _ -> Error ()) entrypoint data  X#**)
    in
    match entrypoint with
    | Some etp -> action etp
    | None -> Error ()

end
