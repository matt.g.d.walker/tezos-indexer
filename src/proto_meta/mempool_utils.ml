(*************************ver****************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(**#X ifdef __TRANSITION__ module Db_alpha = Tezos_indexer_(**# get PROTONEXT#**).Db_alpha X#**)


module Tezos_raw_protocol = Tezos_raw_protocol_(**# get PROTONEXT#**)
module Tezos_protocol = Tezos_protocol_(**# get PROTONEXT#**)
module Tezos_client = Tezos_client_(**# get PROTONEXT#**)

module BS = Block_services.Make(Tezos_protocol.Protocol)(Tezos_protocol_(**# get PROTONEXT#**).Protocol)

module Alpha_context = Tezos_raw_protocol.Alpha_context

module Verbose = Tezos_indexer_lib.Verbose
module Debug = Verbose.Debug

let lazy_expr_pp out e = match Data_encoding.force_decode e with
  | None -> ()
  | Some p ->
    Format.fprintf out "%a"
      Tezos_client.Michelson_v1_printer.print_expr p

let operation_pp : type a. _ -> a Alpha_context.manager_operation -> unit = fun out -> function
  | Alpha_context.Reveal pk ->
    Format.fprintf out "{Reveal %a}" Signature.Public_key.pp pk
  | Transaction { amount ; parameters ; entrypoint ; destination } ->
    Format.fprintf out "{Transaction amount=%a parameters=%a entrypoint=%s destination=%a}"
      Alpha_context.Tez.pp amount
      lazy_expr_pp parameters
      entrypoint
      Alpha_context.Contract.pp destination
  | Origination { delegate ; script ; credit ; preorigination ; } ->
    Format.fprintf out "{Origination%a script=%a credit=%a%a }"
      (Verbose.Utils.pp_option ~name:"delegate" ~pp:Signature.Public_key_hash.pp) delegate
      (fun out { Alpha_context.Script.code ; storage } ->
         Format.fprintf out "{code=%a storage=%a}"
           lazy_expr_pp code
           lazy_expr_pp storage
      ) (script:Alpha_context.Script.t)
      Alpha_context.Tez.pp credit
      (Verbose.Utils.pp_option ~name:"preorigination" ~pp:Alpha_context.Contract.pp) preorigination
  | Delegation pkh_opt ->
    Format.fprintf out "{Delegation%a}"
      (Verbose.Utils.pp_option ~name:"pkh" ~pp:Signature.Public_key_hash.pp) pkh_opt

type mop =
  | R of Signature.public_key
  | T of { amount : Alpha_context.Tez.t
         ; parameters : Alpha_context.Script.lazy_expr
         ; entrypoint : string
         ; destination : Alpha_context.Contract.t
         }
  | O of { delegate : Signature.public_key_hash option
         ; script : Alpha_context.Script.t
         ; credit : Alpha_context.Tez.t
         ; preorigination : Alpha_context.Contract.t option
         ; }
  | D of Signature.public_key_hash option

let mop_of_manager_operation : type a. a Alpha_context.manager_operation -> mop = function
  | Alpha_context.Reveal pk ->
    R pk
  | Transaction { amount ; parameters ; entrypoint ; destination ; } ->
    T { amount ; parameters ; entrypoint ; destination }
  | Origination { delegate ; script ; credit ; preorigination ; } ->
    O { delegate ; script ; credit ; preorigination ; }
  | Delegation pkh_opt ->
    D pkh_opt


(* module Endorsements = struct
 *   module M = Map.Make(struct
 *       type t = Block_hash.t * Alpha_context.Raw_level.t
 *       let compare = compare
 *     end)
 *   let m = ref M.empty in
 *   let seen e = M.is_mem e !m in
 *   let register e = m := M.add e !m in
 * end *)

type status = Db_alpha.Mempool_operations.status = | Applied | Refused | Branch_refused | Unprocessed | Branch_delayed

module Tez = Alpha_context.Tez

type result = {
  age: int;
  branch: Block_hash.t;
  fee: Tez.t;
  operation: mop;
  counter: Z.t;
  gas_limit: Z.t;
  storage_limit: Z.t;
  status: status;
  source: Signature.public_key_hash;
  op_hash: Operation_hash.t;
  seen: float;
}
let string_of_status = function
  | Applied -> "applied"
  | Refused -> "refused"
  | Branch_refused -> "branch_refused"
  | Unprocessed -> "unprocessed"
  | Branch_delayed -> "branch_delayed"
let status_of_string = function
  | "applied" -> Some Applied
  | "refused" -> Some Refused
  | "branch_refused" -> Some Branch_refused
  | "unprocessed" -> Some Unprocessed
  | "branch_delayed" -> Some Branch_delayed
  | _ -> None


module MempoolOperations = struct
  type t = Db_alpha.Mempool_operations.t = {
    branch: Block_hash.t;
    op_hash: Operation_hash.t;
    status: status;
    id: int;
    operation_kind: int;
    source: Signature.public_key_hash option;
    destination: Alpha_context.Contract.t option;
    seen: float;
    json_op: string Lazy.t;
    context_block_level : int32;
  }

  module S = Set.Make(struct
      type t = Db_alpha.Mempool_operations.t
      let compare a b =
        compare
          (a.op_hash, a.id, a.status)
          (b.op_hash, b.id, b.status)
    end)
  let s = ref S.empty
  let new_block () = s := S.empty
  let register ~pool ~bl
      ~json_op
      ~branch ?source ~id ~operation_kind ?destination ~status op_hash =
    let seen = Unix.gettimeofday () in
    let open Db_alpha.Mempool_operations in
    let d = { source ; branch ; operation_kind ; id ; destination ;
              status ; op_hash ; seen ; json_op ; context_block_level = !bl }
    in
    if S.mem d !s then
      Lwt.return (Ok ()) (* already added *)
    else
      begin
        (* register operation into map to avoid duplicates *)
        s:= S.add d !s;
        (* register operation into db *)
        Db_shell.find_opt pool
          Db_alpha.Mempool_operations.insert d
        >>= Db_shell.caqti_or_fail ~__LOC__ >>= fun _ ->
        Lwt.return (Ok ())
      end

end

let int_of_contents :
  type a. a Alpha_context.contents -> int =
  function
  | Endorsement _ -> 0
  | Seed_nonce_revelation _ -> 1
  | Double_endorsement_evidence _ -> 2
  | Double_baking_evidence _ -> 3
  | Activate_account _ -> 4
  | Proposals _ -> 5
  | Ballot _ -> 6
  | Manager_operation { operation; _ } ->
    begin
      match operation with
      | Reveal _ -> 7
      | Transaction _ -> 8
      | Origination _ -> 9
      | Delegation _ -> 10
    end
(**# ifdef __META4__
  | Endorsement_with_slot _ -> 11
  | Failing_noop _ -> 12
#**)

let rec process_contents :
  type a. ?id:int -> bl:int32 ref -> pool:_ -> branch:_ -> status:status -> op_hash:_ -> a Alpha_context.contents_list -> unit =
  fun ?(id=0) ~bl ~pool ~branch ~status ~op_hash cl ->
  match cl with
  | Alpha_context.Single e ->
    begin
      let json_op =
        (* we put this into a lazy value because we process each
           operation multiple times, therefore most of the time, we
           won't need this value *)
        lazy
          (Format.asprintf "%a"
             Data_encoding.Json.pp
             (Data_encoding.Json.construct Alpha_context.Operation.contents_encoding (Contents e)))
      in
      match e with
      | Manager_operation { source ; fee = _ ; operation = Reveal _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; operation = Transaction { destination ; _ } ;
                            fee = _ ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id ~destination
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Origination { delegate ; script = _ ; credit = _ ; preorigination = _ ; } ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ?destination:(Option.map Alpha_context.Contract.implicit_contract delegate)
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Manager_operation { source ; fee = _ ; operation = Delegation pkh_opt ; counter = _ ;
                            gas_limit = _ ; storage_limit = _ } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source  ~status ~id
              ?destination:(Option.map Alpha_context.Contract.implicit_contract pkh_opt)
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Activate_account { id = pkh ; activation_code = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source:(Ed25519 pkh) ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Proposals { source ; period = _ ; proposals = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      | Ballot { source ; period = _ ; proposal = _ ; ballot = _ ; } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~source ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
      (**# ifdef __META4__ | Endorsement_with_slot _ | Failing_noop _ #**)
      | Endorsement { level = _ ; }
      | Seed_nonce_revelation { level = _ ; nonce = _ ; }
      | Double_endorsement_evidence
          { op1 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
            op2 = { shell = { branch = _ } ;
                    protocol_data = { contents = _ ; signature = _ } }  ;
            (**# ifdef __META4__ slot = _ ; #**)
          }
      | Double_baking_evidence
          { bh1 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } ;
            bh2 = { shell = { level = _ ; proto_level = _ ; predecessor = _ ;
                              timestamp = _ ; validation_passes = _ ;
                              operations_hash = _ ; fitness = _ ; context = _ ;
                            } ;
                    protocol_data = { contents = _ ; signature = _ }
                  } } ->
        Lwt.async (fun () ->
            MempoolOperations.register ~pool ~bl ~json_op ~branch ~status ~id
              ~operation_kind:(int_of_contents e) op_hash >>= fun _ -> Lwt.return_unit
          )
    end
  | Cons (hd, tl) ->
    begin
      process_contents ~pool ~bl ~id ~status ~branch ~op_hash (Single hd);
      process_contents ~pool ~bl ~id:(id+1) ~status ~branch ~op_hash tl
    end


let watch_mempool cctxt ~pool newproto bl =
  assert !newproto;
  newproto := false;
  let monitor_pendings () =
    begin
      BS.Mempool.pending_operations cctxt () >>=
      function
      | (Ok BS.Mempool.{ applied; refused; branch_refused; unprocessed; branch_delayed }) ->
        begin
          let treat_one_op i (op_hash, ({shell={branch};protocol_data=Operation_data{contents;signature}}:Tezos_protocol.Protocol.operation)) =
            Debug.printf ~vl:2 "%c (%d) branch=%a op_hash=%a%a" '@' i Block_hash.pp branch Operation_hash.pp op_hash
              (fun out ->
                 function
                 | None -> ()
                 | Some s -> Format.fprintf out " signature=%a" Signature.pp s) signature;
            process_contents ~bl ~pool ~status:Applied ~branch ~op_hash contents
          in
          List.iteri treat_one_op applied;
        end;
        begin
          List.iter (fun (map, status) ->
              Operation_hash.Map.iter (fun op_hash (o, _error_list) ->
                  let treat_one_op op_hash = function (
                    { shell = ({ branch ; } : Operation.shell_header) ;
                      protocol_data = ((Operation_data { contents ; signature ; })
                                       : Alpha_context.packed_protocol_data) }
                    : Alpha_context.packed_operation) ->
                    Debug.printf ~vl:2 "# error=%s op=%a branch=%a%a"
                      (string_of_status status)
                      Operation_hash.pp op_hash Block_hash.pp branch
                      (fun out ->
                         function
                         | None -> ()
                         | Some s -> Format.fprintf out " signature=%a" Signature.pp s) signature;
                    process_contents ~bl ~pool ~status ~branch ~op_hash contents;
                    ()
                  in
                  treat_one_op op_hash o
                )
                map
            )
            [ refused, Refused;
              branch_refused, Branch_refused;
              branch_delayed, Branch_delayed ];
        end;
        begin
          let status = Unprocessed in
          Operation_hash.Map.iter (fun op_hash o ->
              let treat_one_op op_hash = function (
                { shell = ({ branch ; } : Operation.shell_header) ;
                  protocol_data = ((Operation_data { contents ; signature ; })
                                   : Alpha_context.packed_protocol_data) }
                : Alpha_context.packed_operation) ->
                Debug.printf ~vl:2 "# error=%s op=%a branch=%a%a"
                  (string_of_status status)
                  Operation_hash.pp op_hash Block_hash.pp branch
                  (fun out ->
                     function
                     | None -> ()
                     | Some s -> Format.fprintf out " signature=%a" Signature.pp s) signature;
                process_contents ~bl ~pool ~status ~branch ~op_hash contents;
                ()
              in
              treat_one_op op_hash o
            )
            unprocessed
        end;
        Lwt.return (Ok ())
      | Error e ->
        Lwt.return (Error e)
    end
  in
  let rec monitor_ops () =
    begin
      BS.Mempool.monitor_operations ~refused:true ~branch_refused:true cctxt () >>=
      function
      | Error e -> Lwt.return (Error e)
      | Ok (stream, stopper) ->
        MempoolOperations.new_block ();
        Debug.printf ~vl:0 "# BEGIN monitor_operations cycle";
        let rec loop () =
          Lwt_stream.get stream >>= function
          | None ->
            stopper ();
            Debug.printf ~vl:0 "# END monitor_operations cycle";
            Lwt.return (Ok ())
          | Some (_op_list:Tezos_protocol.Protocol.operation list) ->
            (* We ignore the contents of [op_list]. What matters with
               this stream is to get a signal from the node to know
               that there's new operations; and we're using
               BS.Mempool.pending_operations to get full information
               instead of partial information. It's not optimal at all
               but until op_list contains operation hashes, we can't
               avoid using BS.Mempool.pending_operations.  And we
               don't want to use only BS.Mempool.pending_operations
               because that means we don't know when a block is baked,
               and we also loop too much when there's no new
               operation.

               In a future version of tezos-node, this technique will
               be deprecated: instead of ignoring the contents of this
               list, we will - on the contrary - use it and won't need
               to loop on pending_operations.
               https://gitlab.com/tezos/tezos/-/merge_requests/2156 *)
            monitor_pendings () >>= function
            | Ok ()   -> loop ()
            | Error e -> Lwt.return (Error e)
        in
        loop ()
    end
    >>= function
    | Ok () ->
      if !newproto then
        Lwt.return (Ok ())
      else
        monitor_ops ()
    | Error e ->
      Lwt.return (Error e)
  in
  monitor_ops ()
  >>= function
  | Ok () ->
    Lwt.return (Ok ())
  | Error e ->
    Lwt.return (Error e)
