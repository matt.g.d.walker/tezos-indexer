(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

val store_block_full :
  (* depth:int -> *)
  ?use_disk_cache:string ->
  ?tokens:bool ->
  ?chain:Tezos_shell_services.Block_services.chain ->
  ?block:Tezos_shell_services.Block_services.block ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->
  X#**)(**#X else #(**#2 ifdef __TRANSITION__ Tezos_client_(**# get PROTO #**).2#**)Protocol_client_context.full ->X#**)
  (module Caqti_lwt.CONNECTION) ->
  int32 tzresult Lwt.t


val bootstrap_chain :
  ?stepback:int32 ->
  ?use_disk_cache:string ->
  ?from:int32 ->
  ?up_to:int32 ->
  ?tokens:bool ->
  first_alpha_level:int32 ->
  sqltx_rate:int32 ->
  (* depth:int -> *)
  ?threads:int ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->
  X#**)(**#X else #(**#2 ifdef __TRANSITION__ Tezos_client_(**# get PROTO #**).2#**)Protocol_client_context.full ->X#**)
  (module Caqti_lwt.CONNECTION) ->
  (unit -> (module Caqti_lwt.CONNECTION) Lwt.t) ->
  int32 tzresult Lwt.t

val string_of_block_info_from_string : string -> string  (* identity unless it fails *)


(**#! ifndef __TRANSITION__
val record_contract_balance :
  (module Caqti_lwt.CONNECTION) ->
  (**#X ifdef __META1__ #Tezos_client_(**# get PROTO #**).Alpha_client_context.full ->
  X#**)(**#X else #(**#2 ifdef __TRANSITION__ Tezos_client_(**# get PROTO #**).2#**)Protocol_client_context.full ->X#**)
  bhid:Db_shell.bhid ->
  bh:Block_hash.t ->
  level:Db_shell.bl ->
  k:Tezos_raw_protocol_(**# get PROTONEXT#**).Alpha_context.Contract.t ->
  kid:Db_shell.kid ->
  db_max_level:Db_shell.bl ->
  unit tzresult Lwt.t
!#**)
