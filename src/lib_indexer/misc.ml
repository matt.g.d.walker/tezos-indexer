(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs, <contact@nomadic-labs.com>               *)
(* Copyright (c) 2021 Philippe Wang <philippe.wang@gmail.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let flatten_option = function
  | None -> None
  | Some None -> None
  | Some l -> l


let make_counter ?(start=0) () =
  let c = ref start in
  (fun () -> let r = succ !c in c := r ; r)

let make_counter64 ?(start=0L) () =
  let c = ref start in
  (fun () -> let r = Int64.succ !c in c := r; r)

(* let max_int53 = 9007199254740991L *)
let min_int53 = -9007199254740991L

let rec list_assoc_opt e = function
  | (x, y)::_ when e = x -> Some y
  | [] -> None
  | _::tl -> list_assoc_opt e tl

let rec list_iter f = function
  | e::tl -> f e ; list_iter f tl
  | [] -> ()

let rec list_mem e = function
  | x::tl -> (x = e) || list_mem e tl
  | [] -> false


let limited_list_iter_p : ('a -> unit Lwt.t) -> 'a list -> unit Lwt.t = fun f l ->
  let rec process = function
    | e1::e2::e3::e4::e5::e6::e7::e8::e9::e10::e11::e12::tl ->
      Lwt_list.iter_p f [e1; e2; e3; e4; e5; e6; e7; e8; e9; e10; e11; e12]
      >>= fun () -> process tl
    | l -> Lwt_list.iter_p f l
  in
  process l
