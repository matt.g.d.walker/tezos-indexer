(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Vincent Bernardoff <vb@luminar.eu.org>                 *)
(* Copyright (c) 2019 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module RPC_client = Tezos_rpc_http_client_unix.RPC_client_unix

let host_port_of_url url =
  let tls = match Uri.scheme url with
    | None -> false
    | Some "https" -> true
    | Some _ -> false in
  match Uri.host url, Uri.port url with
  | Some host, Some port -> host, port, tls
  | _ -> invalid_arg "host_port_of_url"

let base_dir = Filename.concat (Sys.getenv "HOME") ".tezos-client"
let cfg_file base_dir = Filename.concat base_dir "config"

let mk_rpc_cfg tezos_client_dir url =
  let cfg = cfg_file tezos_client_dir in
  Tezos_client_base_unix.Client_config.read_config_file cfg >>= fun c ->
  match c, url with
  | Error _, None ->
    Verbose.eprintf ~force:true
      "Warning: your tezos-client configuration file (default \
       is %s but you may specify where to look using --tezos-client-dir) \
       could not be read, and you did not \
       specify how to connect to the tezos node RPC server on \
       the command line either (using --tezos-url). \
       I'll try and use http://127.0.0.1:8732\n\
       For more information, run %s --help" (cfg_file base_dir) Sys.argv.(0);
    let host, port, tls = "localhost", 8732, false in
    return (RPC_client.default_config,
            None,
            host,
            port,
            tls
           )
  | _, Some url ->
    let host, port, tls = host_port_of_url url in
    return ({ RPC_client.default_config with endpoint = url },
            None,
            host,
            port,
            tls
           )
  | Ok { node_addr; node_port; tls; confirmations ; endpoint ; _ }, None ->
    let tls = match tls with
      | Some true -> true
      | _ -> match endpoint with
        | None -> false
        | Some e -> Uri.scheme e = Some "https"
    in
    let host = match node_addr with
      | Some h -> h
      | None ->
        match endpoint with
        | None -> "localhost"
        | Some e -> match Uri.host e with
          | None -> "localhost"
          | Some e -> e
    and port = match node_port with
      | Some p -> p
      | None ->
        match endpoint with
        | None -> 8732
        | Some e -> match Uri.port e with
          | None -> if Uri.scheme e = Some "https" then 443 else 80
          | Some e -> e
    in
    return ({ RPC_client.default_config with
              endpoint =
                match endpoint with
                | Some e -> e
                | None ->
                  Uri.of_string @@ Printf.sprintf "http%s://%s:%d" (if tls then "s" else "") host port
            },
            confirmations,
            host,
            port,
            tls
           )


let tezos_indexer_full
    ?(block=`Head 0)
    ?confirmations
    ?password_filename
    ?(base_dir=base_dir)
    ?(rpc_config=RPC_client.default_config)
    () : #Client_context.full = object
  inherit Tezos_client_base_unix.Client_context_unix.unix_full
      ~chain:`Main ~base_dir ~block ~confirmations ~password_filename ~rpc_config
  method! answer :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Verbose.printf "%s" msg;
        Lwt.return_unit)
  method! error :
    type a b. (a, b) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Verbose.eprintf "%s" msg;
        Lwt.fail_with msg)
  method! log : (* never used in practice *)
    type a. string -> (a, unit) Client_context.lwt_format -> a =
    fun _output -> Format.kasprintf (fun msg ->
        Verbose.printf "%s" msg;
        Lwt.return_unit)
  method! message :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Verbose.printf "%s" msg;
        Lwt.return_unit)
  method! warning :
    type a. (a, unit) Client_context.lwt_format -> a =
    Format.kasprintf (fun msg ->
        Verbose.eprintf "%s" msg;
        Lwt.return_unit)
end
