# Open Source License
# Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

docker-build:
	make _docker
	cd _docker && git commit -a -m "dev $$(date)" ; docker build .

docker-pg12:
	make _docker
	cd _docker && sed 's/FROM postgres:alpine as build/FROM postgres:12.7-alpine as build/' Dockerfile-test-postgres > Dockerfile-test-postgres12 && docker build . -f Dockerfile-test-postgres12

docker-pg13:
	make _docker
	mkdir -p _docker && cp Dockerfile-test-postgres _docker
	cd _docker && docker build . -f Dockerfile-test-postgres

docker-all:
	make docker-pg13 docker-pg12 docker-build

_docker:
	rm -fr _docker && mkdir _docker
	cp -fa .git _docker && cd _docker && git checkout .
	cp -af Dockerfile Dockerfile-test-postgres src Makefile _docker/

.PHONY: _docker docker-pg13 docker-pg12 docker-build
